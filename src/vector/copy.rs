pub fn copy<T: Copy>(dest: &mut Vec<T>, src: &Vec<T>) {
  dest.clear();
  for c in src.iter() {
    dest.push(*c);
  }
}

pub fn concat<T: Copy>(dest: &mut Vec<T>, src: & Vec<T>) {
  for c in src.iter() {
    dest.push(*c);
  }
}

pub fn join<T: Copy>(a: & Vec<T>, b: & Vec<T>) -> Vec<T> {
  let mut fresh = a.clone();
  concat(&mut fresh, &b);
  fresh
}