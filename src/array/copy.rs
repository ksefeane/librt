pub fn copy<T: Copy>(dest: &mut [T], src: &[T]) {
  for i in 0..src.len() {
    dest[i] = src[i];
  }
}