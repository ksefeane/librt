use core::fmt::Debug;

pub fn print<T: Debug>(arr: &[T]) {
  for c in arr.iter() {
    print!("{:?}", c);
  }
}

pub fn print_ln<T: Debug>(arr: &[T]) {
  for c in arr.iter() {
    print!("{:?}", *c);
  }
  println!("");
}

pub fn write(s: &[char]) {
  for c in s.iter() {
    print!("{}", c);
  }
}

pub fn write_ln(s: &[char]) {
  write(&s);
  println!("");
}