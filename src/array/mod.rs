pub mod display;
pub mod modify;
pub mod copy;
pub mod find;
pub mod compare;

pub use display::{print, print_ln, write, write_ln};
pub use modify::modify;
pub use copy::copy;
pub use find::findex;
pub use compare::same;
