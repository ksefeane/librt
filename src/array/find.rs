
pub fn findex<T: PartialEq + Copy>(arr: &[T], c: &T) -> Option<usize> {
  let mut i: usize = 0;
  let len = arr.len();
  for j in 0..len {
    if arr[j] == *c {
      i = j;
      break;
    }
  }
  if i > 0 {Some(i)} else {None}
}