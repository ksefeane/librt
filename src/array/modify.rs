pub fn modify<T: Copy>(arr: &mut [T], c: T, n: usize) {
  let mut i = 0;
  while i < n {
    arr[i] = c;
    i += 1;
  }
}