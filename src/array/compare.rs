pub fn same<T: PartialEq>(a: &[T], b: &[T]) -> bool {
  let mut i = false;
  if a.len() == b.len() {
    i = true;
    let len = a.len();
    for j in 0..len {
      if a[j] != b[j] {i = false}
    }
  }
  i
}